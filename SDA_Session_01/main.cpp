//
//  main.cpp
//  SDA_Session_01
//
//  Created by Giulio Iacomino on 07/10/2021.
//  Copyright © 2021 Giulio Iacomino. All rights reserved.
//

#include <iostream>
using namespace std;

int calculateFactorial (int number)
{
    int factorial = 1;
    
    if (number > 0)
    {
            for (int i = number; i != 0; i--)
            {
                factorial *= i;
            }
    }
    
    return factorial;
}



int main(int argc, const char * argv[]) {
    // insert code here...
    int res = 0;
    cout << "Please enter a positive integer number" << endl;
    cin >> res;
    cout << "The factorial of " << res << " is: " << calculateFactorial(res) << "." << endl;
    return 0;
}
